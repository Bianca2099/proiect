const Sequelize = require('sequelize')
const sequelize = new Sequelize('proiect','admin','admin*',{
    dialect:'mysql'
})

const Student= sequelize.define('student',{
    email: Sequelize.STRING,
    password:Sequelize.STRING,
    rol: Sequelize.STRING,
    nume_echipa:  Sequelize.STRING,
    nume_proiect:  Sequelize.STRING,
    cod_bug: Sequelize.STRING,
    stat_bug: Sequelize.STRING
})

const Echipa= sequelize.define('echipa',{
    nume_echipa: Sequelize.STRING,
    nume_proiect: Sequelize.STRING,
    nr_persoane: Sequelize.INTEGER
})

const Proiect= sequelize.define('proiect',{
    nume_proiect:  Sequelize.STRING,
    nume_echipa: Sequelize.STRING
})

const Bug= sequelize.define('bug',{
    severitate: Sequelize.STRING,
    prioritate: Sequelize.STRING,
    descriere:  Sequelize.STRING,
    link: Sequelize.STRING,
    stat_bug: Sequelize.STRING
})

sequelize.sync({force:true})
    .then(()=>console.log('created'))
    .catch((error)=> console.log(error))

const express=require('express')
const bodyParser=require('body-parser')

const Sequelize = require('sequelize')
const sequelize = new Sequelize('proiect','admin','admin*',{
    dialect:'mysql'
})


const Student= sequelize.define('student',{
    email: Sequelize.STRING,
    password:Sequelize.STRING,
    rol: Sequelize.STRING,
    nume_echipa:  Sequelize.STRING,
    nume_proiect:  Sequelize.STRING,
    cod_bug: Sequelize.STRING,
    stat_bug: Sequelize.STRING
})

const Echipa= sequelize.define('echipa',{
    nume_echipa: Sequelize.STRING,
    nume_proiect: Sequelize.STRING,
    nr_persoane: Sequelize.INTEGER
})

const Proiect= sequelize.define('proiect',{
    nume_proiect:  Sequelize.STRING,
    nume_echipa: Sequelize.STRING
})

const Bug= sequelize.define('bug',{
    severitate: Sequelize.STRING,
    prioritate: Sequelize.STRING,
    descriere:  Sequelize.STRING,
    link: Sequelize.STRING,
    stat_bug: Sequelize.STRING
})
const app=express()
app.use(bodyParser.json())

app.post('/create',async(req,res)=>{
    try{
        await sequelize.sync({force:true})
        res.status(200).json({message:'created'})
    }catch(err){
        console.warn(err)
        res.status(500).json({message:'server error'})
    }
})


app.get('/students', async(req,res)=>{
    try{
        let students= await Student.findAll()
        res.status(200).json(students)
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message:'server error'})
        
    }
})      
app.post('/students',async (req,res)=>{
     try{
        await Student.create(req.body)
        res.status(200).json({message: ' created'})
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message:'server error'})
        
    }
  
})

app.get('/students/ :id',async (req,res)=>{
    try{
        let student=await Student.findByPk(req.params.id)
        if(student){
            res.status(200).json(student)
        }else{
             res.status(404).json({message:'server error'})
        }
    }
    catch(e){
        console.warn(e)
         res.status(500).json({message:'server error'})
    }
})
app.put('/students/ :id',async(req,res)=>{
    try{
        let student=await Student.findByPk(req.params.id)
        if(student){
            await student.update(req.body)
            res.status(200).json({message:'created'})
        }else{
             res.status(404).json({message:'server error'})
        } 
    }catch(e){
        console.warn(e)
         res.status(500).json({message:'server error'})
        }
    
    
})
app.delete('/students/ :id',async(req,res)=>{
     try{
        let student=await Student.findByPk(req.params.id)
        if(student){
            await student.destroy(req.body)
            res.status(200).json({message:'created'})
        }else{
             res.status(404).json({message:'server error'})
        } 
    }catch(e){
        console.warn(e)
         res.status(500).json({message:'server error'})
        }
    
})
app.listen(8080);